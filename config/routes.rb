Rails.application.routes.draw do
 
  devise_for :users, controllers: { registrations: "registrations" , sessions: "sessions",
   passwords: "passwords"} 

  devise_scope :user do
    get '/users' => 'registrations#index'
    get '/users/password' => 'passwords#index'
    get '/users/sign_in' => 'sessions#new'
    root to: "home#index"
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  
end
